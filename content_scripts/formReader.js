//var Hashes= require('jshashes');
initialize();
function initialize(){  
    document.addEventListener('submit', readForm);
}

function readForm(){
    var username='';
    var password='';
    //var SHA1 = new Hashes.SHA1();
    for(var i=0; i < document.forms[0].length; i++){
        var form_field=document.forms[0].elements[i];
        if(form_field.getAttribute('type')=='text'){
            username=form_field.value;
        }
        else if(form_field.getAttribute('type')=='password'){
            password=form_field.value;
        }
    }
    console.log("Username:",username,"\tPassword:",password);
    //console.log("Hashed Password:", SHA1.hex(password));
    saveForm(username,password);
}

function saveForm(username,password){
    chrome.storage.local.set({ [username] : password}, function(){
        if(chrome.runtime.lastError){
            console.log(chrome.runtime.lastError);
        } else {
            console.log('Storage successful');
        }
    });
}
