document.addEventListener("click", function(e) {
  if (!e.target.classList.contains("fill")) {
    return;
  }

chrome.tabs.executeScript(null, {
    file: "/content_scripts/fillform.js"
  });

});
